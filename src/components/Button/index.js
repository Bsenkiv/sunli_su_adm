import React from 'react'

const Button = ({ text, onClick, className, disabled, style }) => {
    return (
        <button onClick={onClick} className={`button ${className}`} disabled={disabled} style={style} >
            <p>{text}</p>
        </button>
    )
}

export default Button;
