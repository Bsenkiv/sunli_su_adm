import React from 'react';
import { Button } from '..';
import Close from '../../assets/svg/close';

const Modal = ({
    isVisible,
    setVisible,
    firstButtonText,
    firstButtonPress,
    secondButtonText,
    secondButtonPress,
    children,
    conteinerStyle
}) => {
    if (isVisible) {
        return (
            <div className='modal-container'>
                <div className='modal-custom-block' style={conteinerStyle}>
                    <div className='close-button-modal-block'><span  onClick={() => setVisible(!isVisible)}><Close /></span></div>
                    {children}
                    {(secondButtonText || firstButtonText) &&
                        <div className='edit-profile-bottom-block' style={{ marginTop: 30 }}>
                            {firstButtonText && <Button className={'bordered-button'} text={firstButtonText} style={{ width: '100%' }} onClick={firstButtonPress} />}
                            <div style={{ width: 20 }} />
                            {secondButtonText && <Button text={secondButtonText} style={{ width: '100%' }} onClick={secondButtonPress} />}
                        </div>}
                </div>
            </div>
        )
    } else {
        return null;
    }
}

export default Modal;