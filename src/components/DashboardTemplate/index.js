import React, { useState } from 'react'
import { useSelector } from 'react-redux';
import { Link, NavLink } from 'react-router-dom'
import { axiosService } from '../../api';
import Bell from '../../assets/svg/bell';
import BlueLogout from '../../assets/svg/blueLogout';
import Chats from '../../assets/svg/chats';
import Logout from '../../assets/svg/logout'
import MyAppointments from '../../assets/svg/MyAppointments';
import User from '../../assets/svg/user';
import Users from '../../assets/svg/users';
import VerticalDots from '../../assets/svg/verticalDots';
import { API_URL } from '../../constants';
import { userSelector } from '../../store/user/selectors';
import Modal from '../Modal';

const DashbordTemplate = ({ children }) => {
    const { user } = useSelector(userSelector);
    const [logoutVisible, setLogoutVisible] = useState(false);
    const tabs = [
        { title: 'Appointments', url: 'dashboard/appointments', image: <MyAppointments /> },
        { title: 'Patients', url: 'dashboard/patients', image: <Users /> },
        { title: 'Chats', url: 'dashboard/chats', image: <Chats /> },
        { title: 'My Profile', url: 'dashboard/my-profile', image: <User /> },
    ];

    const logout = () => {
        axiosService.get(API_URL + 'signout').then(() => {
            localStorage.removeItem('token');
            localStorage.removeItem('user');
            window.location.pathname = '/'
        })
    }

    return (
        <div>
            <div className='header' style={{ paddingLeft: 150, justifyContent: 'flex-end' }}>
                <img alt='avatar' style={{ width: 38 }} src={require('../../assets/images/avatar.png')} />
                <span className='header-user-name'>{user?.name} {user?.surname}</span>
                <span style={{ marginRight: 30 }}><Bell /></span>
                <VerticalDots />
            </div>
            <div className='sidebar'>
                <Link className='logo' to="/">Sunlisant</Link>
                <div style={{ marginTop: 26 }}>
                    {tabs.map((el, index) => (
                        <NavLink key={index} className="block" to={`/${el.url}`}>
                            <div>
                                <div className='centered' style={{ marginBottom: '1.3vh' }}>
                                    {el.image}
                                </div>
                                {el.title}
                            </div>
                        </NavLink>
                    ))}
                </div>
                <div style={{ marginBottom: 50 }} className="block logout-sidebar" onClick={() => setLogoutVisible(true)}>
                    <div>
                        <div className='centered' style={{ marginBottom: '1.3vh' }}>
                            <Logout />
                        </div>
                        Log out
                    </div>
                </div>
            </div>
            <div style={{ marginLeft: 150, marginTop: 75, padding: 50 }}>
                <Modal
                    isVisible={logoutVisible}
                    setVisible={setLogoutVisible}
                    firstButtonText={'Cancel'}
                    firstButtonPress={() => setLogoutVisible(false)}
                    secondButtonText={'Log out'}
                    secondButtonPress={logout}
                    conteinerStyle={{ width: 430, textAlign: 'center' }}
                >
                    <div style={{display: 'flex', justifyContent: 'center', marginBottom: 25}}>
                        <BlueLogout />
                    </div>
                    <div className='logout-modal-text'>Do you want to log out?</div>
                </Modal>
                {children}
            </div>
        </div>
    )
}

export default DashbordTemplate
