import React from 'react'
import { Link, useLocation, useNavigate } from 'react-router-dom'
import { Button } from '..';

const AuthTemplate = ({ children }) => {
    const navigate = useNavigate();
    const location = useLocation();

    return (
        <div>
            <div className='header' >
                <Link className='logo' to="/">Sunlisant</Link>
                {location.pathname.split('/')[1] === 'forgot-password' && <Button className={'bordered-button'} text={'Sign In'} style={{height: 44, width: 105}} onClick={() => navigate('signin')} />}
            </div>
            <div style={{ paddingTop: 75 }}>
                {children}
            </div>
        </div>
    )
}

export default AuthTemplate