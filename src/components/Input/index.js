import React, { useState } from 'react';
import HidePassword from '../../assets/svg/hidePassword';
import ShowPassword from '../../assets/svg/showPassword';

const Input = ({ placeholder, onChange, value, textStyle, title, containerStyle, inputStyle, containerClass, type, isPassword, error }) => {
    const [passwordVisible, setPasswordVisible] = useState(false);
    const [focus, setFocus] = useState(false);
    let color = error ? '#FF6771' : focus ? '#0079BC' : '#ACACAC';

    return (
        <div className={`${containerClass ? containerClass : ''}`} style={containerStyle}>
            {title && <div className='label' style={{ ...textStyle, color: color }}>{title}</div>}
            <div className='input-container' style={{ borderColor: color }}>
                <input
                    onFocus={() => setFocus(true)}
                    onBlur={() => setFocus(false)}
                    className='input'
                    placeholder={placeholder}
                    onChange={(e) => onChange(e.target.value)}
                    value={value} type={isPassword ? !passwordVisible && type : type}
                    style={{ ...textStyle, inputStyle }}
                />
                {isPassword && <span onClick={() => setPasswordVisible(!passwordVisible)}>{passwordVisible ? <HidePassword /> : <ShowPassword />}</span>}
            </div>
            <div style={{ color: color, marginTop: 10 }} className={'error-text'}>{error}</div>
        </div>
    )
}

export default Input;
