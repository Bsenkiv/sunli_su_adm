import React, { useState } from 'react';
import Select from "react-dropdown-select";

const DropDown = ({ options, defaultValue, onChange, label, placeholder, error }) => {
    const [focus, setFocus] = useState(false);
    let color = error ? '#FF6771' : focus ? '#0079BC' : '#ACACAC';

    return (
        <div style={{ flex: 1 }}>
            <div className='label' style={{ fontSize: 14 }}>{label}</div>
            <Select
                placeholder='Specialization'
                style={{ borderWidth: 0, borderBottomWidth: 2, boxShadow: "none", borderColor: '#ACACAC', fontSize: 14, height: 50, paddingLeft: 0, fontFamily: "Montserrat-Medium" }}
                defaultValue={defaultValue}
                onChange={onChange}
                options={options}
            />
        </div>
    )
}

export default DropDown;
