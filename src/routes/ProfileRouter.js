import React from "react";
import { Route, Routes } from "react-router-dom";
import Profile from "../pages/Profile";
import ChangePassword from "../pages/Profile/ChangePassword";
import PrivacyPolicy from "../pages/Profile/PrivacyPolicy";
import ResetPassword from "../pages/Profile/ResetPassword";
import TermsConditions from "../pages/Profile/TermsConditions";

const ProfileRouter = () => {
    return (
        <Routes>
            <Route index path="" element={<Profile />} />
            <Route path="change-password" name="change-password" element={<ChangePassword />} />
            <Route path="reset-password" name="reset-password" element={<ResetPassword />} />
            <Route path="privacy-policy" name="privacy-policy" element={<PrivacyPolicy />} />
            <Route path="terms-conditions" name="terms-conditions" element={<TermsConditions />} />
        </Routes>
    )
}

export default ProfileRouter;