import React from "react";
import { Route, Routes } from "react-router-dom";
import Appointments from "../pages/Appointments";


const AppointmentsRouter = () => {
    return (
        <Routes>
            <Route index path="" name="appoinments" element={<Appointments />} />
        </Routes>
    )
}

export default AppointmentsRouter;