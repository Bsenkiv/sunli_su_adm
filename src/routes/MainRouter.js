import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import {BrowserRouter, Navigate, Route, Routes} from 'react-router-dom';
import { axiosService } from '../api';
import { userSelector } from '../store/user/selectors';
import AuthRouter from './AuthRouter';
import DashboardRouter from './DashboardRouter';
import {axiosSetToken} from "../api/request";

const MainRouter = () => {
    const { token } = useSelector(userSelector);

    useEffect(() => {
        if (token) {
            axiosSetToken(token)
        }
    }, [token])

    return (
        <BrowserRouter>
            <Routes>
                <Route path="/*" element={!token ? <AuthRouter /> : <Navigate to={'/dashboard/my-profile'} />} />
                <Route path="/dashboard/*" element={token ? <DashboardRouter /> : <Navigate to={'/signin'} />} />
            </Routes>
        </BrowserRouter>
    )
}

export default MainRouter;
