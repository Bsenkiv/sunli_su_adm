import React from "react";
import { Navigate, Outlet, Route, Routes } from "react-router-dom";
import DashboardTemplate from "../components/DashboardTemplate";
import ChangePassword from "../pages/Profile/ChangePassword";
import AppointmentsRouter from "./AppointmentsRouter";
import ProfileRouter from "./ProfileRouter";

const DashboardRouter = () => {
    function RequireRoleProvider() {
        let token = localStorage.getItem('token');

        if (!token) {
            return <Navigate to={'/signin'} />;
        }

        return <Outlet />;
    }

    return (
        <DashboardTemplate>
            <Routes>
                <Route element={<RequireRoleProvider />}>
                    <Route index path="my-profile/*" element={<ProfileRouter />} />
                    <Route path="chats" name="chats" element={null} />
                    <Route path="appointments" name="chats" element={<AppointmentsRouter />} />
                    <Route path="change-password" element={<ChangePassword />} />
                </Route>
            </Routes>
        </DashboardTemplate>
    )
}

export default DashboardRouter;