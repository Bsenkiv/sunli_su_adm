import React from "react";
import { Navigate, Outlet, Route, Routes } from "react-router-dom";
import AuthTemplate from "../components/AuthTemplate";
import EmailVerification from "../pages/Auth/EmailVerification";
import ForgotPassword from "../pages/Auth/ForgotPassword";
import Login from "../pages/Auth/Login";
import PasswordChanged from "../pages/Auth/PasswordChanged";
import ResetPassword from "../pages/Auth/ResetPassword";
import Onboarding from "../pages/Onboarding";

const AuthRouter = () => {
    function RequireRoleProvider() {
        let token = localStorage.getItem('token');

        if (token) {
            return <Navigate to={'dashboard/my-profile'} />;
        }

        return <Outlet />;
    }

    return (
        <AuthTemplate>
            <Routes>
                <Route element={<RequireRoleProvider />}>
                    <Route path="/" element={<Onboarding />} />
                    <Route index path="/signin" element={<Login />} />
                    <Route path="/forgot-password" element={<ForgotPassword />} />
                    <Route path="/forgot-password/email-verification" element={<EmailVerification />} />
                    <Route path="/forgot-password/reset-password/*" element={<ResetPassword />} />
                    <Route path="/password-changed" element={<PasswordChanged />} />
                </Route>
            </Routes>
        </AuthTemplate>
    )
}

export default AuthRouter;