import React from 'react';
import ReactDOM from "react-dom/client";
import reportWebVitals from './reportWebVitals';
import MainRouter from './routes/MainRouter';
import { Provider } from "react-redux";
import store from "./store";
import './styles/index.scss'
import './styles/main.styles.css'

ReactDOM.createRoot(document.querySelector("#root")).render(
  <React.StrictMode>
    <Provider store={store}>
      <MainRouter />
    </Provider>
  </React.StrictMode>
);

reportWebVitals();
