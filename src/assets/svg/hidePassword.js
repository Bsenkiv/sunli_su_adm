import React from 'react'

const HidePassword = () => {
    return (
        <svg width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M2.75 13.75C2.75 13.75 7.5625 19.25 16.5 19.25C25.4375 19.25 30.25 13.75 30.25 13.75" stroke="#ACACAC" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
            <path d="M5.5 16.012L2.75 19.2501" stroke="#ACACAC" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
            <path d="M30.2504 19.2501L27.5059 16.0161" stroke="#ACACAC" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
            <path d="M12.2567 18.8101L11 22.6876" stroke="#ACACAC" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
            <path d="M20.7119 18.821L22.0003 22.6875" stroke="#ACACAC" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
        </svg>
    )
}

export default HidePassword;