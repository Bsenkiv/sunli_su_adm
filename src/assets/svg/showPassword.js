import React from 'react'

const ShowPassword = () => {
    return (
        <svg width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M30.1091 15.2072C30.8033 16.1024 30.8033 17.3108 30.1091 18.2045C27.9227 21.0183 22.6769 26.8121 16.5524 26.8121C10.4279 26.8121 5.18215 21.0183 2.99568 18.2045C2.65794 17.7759 2.47461 17.2486 2.47461 16.7059C2.47461 16.1631 2.65794 15.6358 2.99568 15.2072C5.18215 12.3934 10.4279 6.59961 16.5524 6.59961C22.6769 6.59961 27.9227 12.3934 30.1091 15.2072V15.2072Z" stroke="#ACACAC" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
            <path d="M16.5517 21.0375C18.9781 21.0375 20.9451 19.0983 20.9451 16.7063C20.9451 14.3142 18.9781 12.375 16.5517 12.375C14.1252 12.375 12.1582 14.3142 12.1582 16.7063C12.1582 19.0983 14.1252 21.0375 16.5517 21.0375Z" stroke="#ACACAC" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
        </svg>
    )
}

export default ShowPassword;