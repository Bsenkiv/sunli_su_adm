export const API_URL = 'http://208.109.35.5:3030/adm/';

export const  API_LINKS = { 
    login: 'signin',
    forgotPasswordEmail: 'reset-password',
    checkResetToken: 'check-token/'
}