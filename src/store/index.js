import { combineReducers, configureStore } from "@reduxjs/toolkit";
import userSlice from './user'

let middlewares = [];

const rootReducer = combineReducers({
  userReducer: userSlice,
})

const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }).concat(middlewares)
});

export default store;
