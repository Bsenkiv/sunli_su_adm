import { createSlice } from "@reduxjs/toolkit";
import { forgotPasswordEmail, userLogin, } from "./actions";

const initialState = {
    isLogined: false,
    errorLogin: null,
    error: null,
    // isLoading: false,
    passwordChanged:false,
    errorForgotPassword: '',
    token: localStorage.getItem('token'),
    user: localStorage.getItem('user') && JSON.parse(localStorage.getItem('user')),
    currentPassword: null,
    isLoader:false
}

export const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        resetErrorLogin(state) {
            state.errorLogin = '';
        },
        resetPhoneChecked(state) {
            state.isPhoneChecked = false;
        },
        setPasswordChanged(state, action) {
            state.passwordChanged = action.payload;
        },
        setCurrentPassword(state, action) {
            state.currentPassword = action.payload;
        },
        setLoader(state, action) {
            state.isLoader = action.payload;
        }
    },
    extraReducers: {
        [userLogin.pending.type]: (state) => {
            state.errorLogin = '';
            state.isLoader = true
        },
        [userLogin.fulfilled.type]: (state, action) => {
            state.user = action.payload.data;
            state.token = action.payload.token;
            state.errorLogin = null;
            state.isLoader = false

        },
        [userLogin.rejected.type]: (state, action) => {
            state.errorLogin = action.payload
            state.isLoader = false
        },

        [forgotPasswordEmail.pending.type]: (state) => {
            state.errorForgotPassword = '';
            state.isLoader = true
        },
        [forgotPasswordEmail.fulfilled.type]: (state, action) => {
            state.errorForgotPassword = '';
            state.isLoader = false
        },
        [forgotPasswordEmail.rejected.type]: (state, action) => {
            state.errorForgotPassword = action.payload
            state.isLoader = false
        },
    }
})

export default userSlice.reducer;