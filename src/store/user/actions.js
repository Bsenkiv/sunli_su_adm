import { createAsyncThunk } from "@reduxjs/toolkit";
import { api } from "../../api";

export const userLogin = createAsyncThunk(
    'user/login',
    async ({ email, password, navigate }, thunkAPI) => {
        try {
            const response = await api.login({ email, password });
            
            // console.log(response);
            navigate();
            
            localStorage.setItem('user', JSON.stringify(response.data.data));
            localStorage.setItem('token', response.data.token);
            return response.data;
        } catch (e) {
            return thunkAPI.rejectWithValue({ [e.response.data.type]: e.response.data.msg })
        }
    }
);

export const forgotPasswordEmail = createAsyncThunk(
    'user/forgotPasswordEmail',
    async ({ email, navigate }, thunkAPI) => {
        try {
            const response = await api.forgotPasswordEmail({ email });
            navigate();

            return response.data;
        } catch (e) {
            return thunkAPI.rejectWithValue({ [e.response.data.type]: e.response.data.msg })
        }
    }
);
