import { request, axiosService } from './request';
import { API_LINKS } from '../constants';

export { axiosService };

export const api = {
    login: (params) => request.post(API_LINKS.login, params),
    forgotPasswordEmail: (params) => request.post(API_LINKS.forgotPasswordEmail, params),
    checkResetToken: (params) => request.get(API_LINKS.checkResetToken+ params.token)
};
