import axios from 'axios';

// import { config } from '../../config/config.debug';

export const axiosService = axios.create({
  baseURL: 'http://208.109.35.5:3030/adm',
});

export const axiosSetToken = (token) => {
  axiosService.defaults.headers.common.Authorization = `Bearer ${token}`;
}

export const request = {
  get: (path, params = {}) => axiosService.get(path, params),
  post: (path, params = {}) => axiosService.post(path, params),
  put: (path, params = {}) => axiosService.put(path, params),
  patch: (path, params = {}) => axiosService.patch(path, params),
  delete: (path, params = {}) => axiosService.delete(path, { data: params }),
};
