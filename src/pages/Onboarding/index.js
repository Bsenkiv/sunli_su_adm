
import React, { useRef, useState } from 'react'
import Button from '../../components/Button'
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import hospital from '../../assets/svg/hospital';
import nurse from '../../assets/svg/nurse';
import notepad from '../../assets/svg/notepad';
import BackArrow from '../../assets/svg/back-arrow';
import { useNavigate } from 'react-router-dom';

const Onboarding = (props) => {
    const sliderRef = useRef();
    const [currentSlideIndex, setCurrentSlideIndex] = useState(0);
    const navigate = useNavigate();

    const settings = {
        dots: false,
        infinite: true,
        arrows: false,
        speed: 500,
        accessibility: false,
        slidesToShow: 1,
        slidesToScroll: 1
    };

    const data = [
        {
            image: hospital,
            title: 'Welcome to Sunlisant clinic!',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco . '
        },
        {
            image: nurse,
            title: 'Work with professionals',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco . '
        },
        {
            image: notepad,
            title: 'Easily organize your appointments',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco . '
        }
    ];

    const RenderButtons = () => {
        return (
            <div className='buttons-container'>
                <Button text={'Continue'} onClick={() => { currentSlideIndex === 2 ? navigate('/signin') : sliderRef.current.slickNext() }} />
                <div>
                    <p onClick={() => navigate('/signin')} className='skip-button-slider'>Skip</p>
                </div>
            </div>
        )
    }


    const RenderItem = ({ title, description, image }) => {
        return (
            <div style={{ marginTop: '2.7vh' }}>
                {image()}
                <span style={{ marginTop: '4.5vh' }} className='title'>{title}</span>
                <p className='slider-description'>{description}</p>
            </div>
        )
    }

    return (
        <div style={{ textAlign: 'center' }}>
            <div className="back-slide-container">
                {!!currentSlideIndex ? <>
                    <BackArrow />
                    <span onClick={() => sliderRef.current.slickPrev()}>Back</span>
                </> : null}
            </div>
            <Slider
                afterChange={(index) => setCurrentSlideIndex(index)}
                ref={sliderRef}
                {...settings}
            >
                {data.map((item, index) => <RenderItem key={index} {...item} />)}
            </Slider>
            <RenderButtons />
            <div style={{ marginTop: '8vh', display: 'flex', alignItems: "center", justifyContent: 'center', marginBottom: 25 }}>
                {data.map((el, index) => (
                    <div
                        key={index}
                        style={{ backgroundColor: currentSlideIndex === index ? '#46CD07' : '#C4C4C4', width: currentSlideIndex === index ? 22 : 8 }}
                        className='slider-dor'
                        onClick={() => sliderRef.current.slickGoTo(index)}
                    />
                ))}
            </div>
        </div>
    )
}

export default Onboarding