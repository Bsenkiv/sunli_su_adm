import React, { useEffect, useState } from 'react'
import {useDispatch, useSelector} from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { axiosService } from '../../../api';
import BackArrow from '../../../assets/svg/back-arrow';
import Cancel from '../../../assets/svg/cancel';
import Check from '../../../assets/svg/check';
import Ok from '../../../assets/svg/ok';
import { Button, Input } from '../../../components';
import Modal from '../../../components/Modal';
import { API_URL } from '../../../constants';
import { userSlice } from '../../../store/user';
import {CustomLoader} from "../../../assets/loader/loader";
import {userSelector} from "../../../store/user/selectors";

const ResetPassword = () => {
    const [confirmPassword, setConfirmPassword] = useState('');
    const [password, setPassword] = useState('');
    const navigate = useNavigate();
    const dispatch = useDispatch()
    const [changedPasswordModal, setChangePasswordModal] = useState();
    const [showLoader, setLoader] = useState(false)

    const {currentPassword, isLoader}= useSelector(userSelector)

    const [errors, setErrors] = useState(
        {
            characters: { title: '6 characters minimum', active: false },
            numbers: { title: 'At least 2 numbers', active: false },
            match: { title: 'Passwords should match', active: false },
            latin: { title: 'The password should include latin letters only', active: false },
            checkCurrPassword: {title:'The new password matches with old password', active:false}
        });

    useEffect(() => {
        if (password.length) {
            if (!/^[A-Za-z0-9]+$/.test(password)) {
                setErrors(prevState => ({ ...prevState, latin: { ...prevState.latin, active: false } }))
            } else {
                setErrors(prevState => ({ ...prevState, latin: { ...prevState.latin, active: true } }))
            }


            if (password.length < 6) {
                setErrors(prevState => ({ ...prevState, characters: { ...prevState.characters, active: false } }))
            } else {
                setErrors(prevState => ({ ...prevState, characters: { ...prevState.characters, active: true } }))
            }

            if (password.length - password.replace(/\d/gm, '').length < 2) {
                setErrors(prevState => ({ ...prevState, numbers: { ...prevState.numbers, active: false } }))
            } else {
                setErrors(prevState => ({ ...prevState, numbers: { ...prevState.numbers, active: true } }))
            }

            if (password !== confirmPassword) {
                setErrors(prevState => ({ ...prevState, match: { ...prevState.match, active: false } }))
            } else {
                setErrors(prevState => ({ ...prevState, match: { ...prevState.match, active: true } }))
            }

            if(currentPassword === password){
                setErrors(prevState => ({ ...prevState, checkCurrPassword: { ...prevState.checkCurrPassword, active: false } }))
            }else {
                setErrors(prevState => ({ ...prevState, checkCurrPassword: { ...prevState.checkCurrPassword, active: true } }))
            }

        } else {
            setErrors(prevState => ({ ...prevState, latin: { ...prevState.latin, active: false } }))
        }

    }, [password, confirmPassword]);


    const resetPassword = async () => {
        dispatch(userSlice.actions.setLoader(true))
        axiosService.put(API_URL + 'password', { password })
            .then((response) => {
                if (response.data.err) {
                    dispatch(userSlice.actions.setLoader(false))
                    console.log(response.data.msg)
                    // setErrorModalVisible(true)
                } else {
                    // setChangePasswordModal(true)
                    dispatch(userSlice.actions.setPasswordChanged(true))
                    navigate('/dashboard/my-profile');
                    dispatch(userSlice.actions.setLoader(false))
                }
            })
    }


    return (
        <div>
            <Modal
                isVisible={changedPasswordModal}
                setVisible={setChangePasswordModal}
                conteinerStyle={{ width: 430, textAlign: 'center' }}
            >
                <div style={{ display: 'flex', justifyContent: 'center', marginBottom: 20 }}>
                    <Ok width={55} />
                </div>
                <div className='logout-modal-text'>Password changed successfully</div>
            </Modal>
            <div className="back-slide-container-dashboard">
                <BackArrow />
                <span onClick={() => navigate(-1)}>Back</span>
            </div>
            <div className='centered-container' style={{ marginTop: 0 }}>
                {isLoader && <CustomLoader/>}
                <div>
                    <span className='title'>Enter new password</span>

                    <Input isPassword containerStyle={{ marginTop: 75 }} placeholder={'Password'} onChange={setPassword} value={password} title={'New password'} type={'password'} />
                    <Input isPassword containerStyle={{ marginTop: 45 }} placeholder={'Confirm password'} onChange={setConfirmPassword} value={confirmPassword} title={'Confirm password'} type={'password'} />
                    <div>
                        {Object.entries(errors).map((el, index) => (
                            <div key={index} style={{ marginTop: '2.5vh', display: 'flex', alignItems: 'center' }}>
                                {!el[1].active ? <Cancel /> : <Check />}
                                <span className='errors-password' style={{ color: el[1].active ? '#414141' : '#ACACAC' }}>{el[1].title}</span>
                            </div>
                        ))}
                    </div>
                    <div className='bottom-button-container' style={{ marginTop: '4.5vh' }}>
                        <Button text={'Continue'} disabled={Object.entries(errors).filter(el => !el[1].active).length} onClick={resetPassword} />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ResetPassword
