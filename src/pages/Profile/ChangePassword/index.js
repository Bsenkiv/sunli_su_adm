import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { axiosService } from '../../../api';
import BackArrow from '../../../assets/svg/back-arrow';
import { Button, Input } from '../../../components';
import { API_URL } from '../../../constants';
import {CustomLoader} from "../../../assets/loader/loader";
import {userSlice} from "../../../store/user";
import {useDispatch, useSelector} from "react-redux";
import {userSelector} from "../../../store/user/selectors";

const ChangePassword = () => {
    const navigate = useNavigate();
    const [password, setPassword] = useState('');
    const [errorPassword, setErrorPassword] = useState('');

    const dispatch = useDispatch()
    const checkPassword = async () => {
        dispatch(userSlice.actions.setLoader(true))
        axiosService.post(API_URL + 'check-password', { password })
            .then((response) => {
                if (response.data.err) {
                    setErrorPassword(response.data.msg)
                    dispatch(userSlice.actions.setLoader(true))
                } else {
                    setErrorPassword('')
                    navigate('../reset-password')
                    dispatch(userSlice.actions.setLoader(true))
                }
            })
    }

    return (
        <div>
            <div className="back-slide-container-dashboard">
                <BackArrow />
                <span onClick={() => navigate(-1)}>Back</span>
            </div>
            <div className='centered-container' style={{ marginTop: 0 }}>
                <div className='change-password-block' >
                    <div>
                        <span className='title'>Enter your current password</span>
                        <Input
                            error={errorPassword}
                            isPassword
                            containerStyle={{ marginTop: 85 }}
                            placeholder={'Password'}
                            onChange={setPassword}
                            value={password}
                            title={'Password'}
                            type={'password'} />
                        <span className='forgot-password' onClick={() => navigate('/forgot-password')} >Forgot password?</span>
                    </div>
                    <div className='bottom-button-container' style={{ alignItems: 'flex-end' }}>
                        <Button text={'Continue'} disabled={(password.length < 6)} onClick={checkPassword} />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ChangePassword
