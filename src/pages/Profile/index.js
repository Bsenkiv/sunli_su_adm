
import moment from 'moment';
import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import ArrowRight from '../../assets/svg/arrow-right';
import Close from '../../assets/svg/close';
import Edit from '../../assets/svg/edit'
import Star from '../../assets/svg/star';
import { Button, Input } from '../../components';
import { userSelector } from '../../store/user/selectors';
import Select from "react-dropdown-select";
import DropDown from '../../components/DropDown';
import Ok from '../../assets/svg/ok';
import Modal from '../../components/Modal';
import { userSlice } from '../../store/user';

const Profile = () => {
    const navigate = useNavigate();
    const [editModalVisible, setEditModalVisible] = useState(false);
    const { user, token, passwordChanged } = useSelector(userSelector);
    const [selectedOption, setSelectedOption] = useState(null);
    const dispatch = useDispatch();

    const settings = [
        { title: 'Change password', url: 'change-password' },
        { title: 'Terms & Conditions', url: 'terms-conditions' },
        { title: 'Privacy Policy', url: 'privacy-policy' }
    ];

    const options = [
        { value: 'chocolate', label: 'Chocolate' },
        { value: 'strawberry', label: 'Strawberry' },
        { value: 'vanilla', label: 'Vanilla' },
    ];

    const renderInfoColumns = () => {
        let arr = [!!user?.birth_of_date, !!user?.phone, !!user?.email];

        return `columns-${arr.filter(el => el).length}-info-block`
    }

    if (token) {
        return (
            <div >
                <Modal
                    isVisible={passwordChanged}
                    setVisible={() => dispatch(userSlice.actions.setPasswordChanged(false))}
                    conteinerStyle={{ width: 430, textAlign: 'center' }}
                >
                    <div style={{ display: 'flex', justifyContent: 'center', marginBottom: 20 }}>
                        <Ok width={55} />
                    </div>
                    <div className='logout-modal-text'>Password changed successfully</div>
                </Modal>
                {editModalVisible && <div className='modal-container-edit'>
                    <div className={'block'}>
                        <div className='edit-profile-title'>Edit personal information<span onClick={() => setEditModalVisible(!editModalVisible)}><Close /></span></div>
                        <div className='edit-profile-avatar-block' >
                            <img alt='avatar' src={require('../../assets/images/avatar.png')} style={{ width: 130, height: 130 }} />
                        </div>
                        <div style={{ flexDirection: 'row', display: 'flex', marginTop: 30 }}>
                            <Input containerStyle={{ width: 240 }} textStyle={{ fontSize: 14 }} title={'First name'} placeholder={'First name'} />
                            <div style={{ width: 20 }} />
                            <Input containerStyle={{ width: 240 }} textStyle={{ fontSize: 14 }} title={'Last name'} placeholder={'Last name'} />
                        </div>
                        <div className='label' style={{ fontSize: 14, marginTop: 20 }}>Specialization</div>
                        <Select
                            placeholder='Specialization'
                            style={{ borderWidth: 0, borderBottomWidth: 2, boxShadow: "none", borderColor: '#ACACAC', fontSize: 14, height: 50, paddingLeft: 0, fontFamily: "Montserrat-Medium" }}
                            defaultValue={selectedOption}
                            onChange={setSelectedOption}
                            options={options}
                        />
                        <div style={{ flexDirection: 'row', display: 'flex', marginTop: 30 }}>
                            <DropDown
                                label={'Gender'}
                                placeholder={'Gender'} />
                            <div style={{ width: 20 }} />
                            <div style={{ flex: 1 }}>
                                <div className='label' style={{ fontSize: 14 }}>Specialization</div>
                                <Select
                                    placeholder='Specialization'
                                    style={{ borderWidth: 0, borderBottomWidth: 2, boxShadow: "none", borderColor: '#ACACAC', fontSize: 14, height: 50, paddingLeft: 0, fontFamily: "Montserrat-Medium" }}
                                    defaultValue={selectedOption}
                                    onChange={setSelectedOption}
                                    options={options}
                                />
                            </div>
                        </div>
                        <Input style={{ width: '100%' }} textStyle={{ fontSize: 14 }} containerStyle={{ marginTop: 30 }} title={'Phone number'} placeholder={'Phone number'} />
                        <Input style={{ width: '100%' }} textStyle={{ fontSize: 14 }} containerStyle={{ marginTop: 30 }} title={'Email'} placeholder={'Email'} />
                        <div className='edit-profile-bottom-block'>
                            <Button className={'bordered-button'} text={'Cancel'} style={{ width: '100%' }} onClick={() => setEditModalVisible(!editModalVisible)} />
                            <div style={{ width: 20 }} />
                            <Button text={'Save'} style={{ width: '100%' }} />
                        </div>
                    </div>
                </div>}
                <div className='page-title'>Profile</div>
                <div className={`profile-info-block ${renderInfoColumns()}`}>
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        <img alt='avatar' src={require('../../assets/images/avatar.png')} />
                        <div style={{ marginLeft: 25 }}>
                            <div className='info-user-name'>{user.name} {user.surname}</div>
                            <div className='info-user-specialty'>{user.position}</div>
                            <div className='info-user-reviews'><Star /><span>4.8</span> 321 Reviews</div>
                        </div>
                    </div>
                    {user?.birth_of_date && <div>
                        <div className='info-title'>Birthday</div>
                        <div className='info-description'>{moment().diff(user.birth_of_date, 'years')} years ({moment(user.birth_of_date).format('DD.MM.YYYY')})</div>
                    </div>}
                    {user?.phone && <div>
                        <div className='info-title'>Phone number</div>
                        <div className='info-description'>{user?.phone}</div>
                    </div>}
                    {user?.email && <div>
                        <div className='info-title'>Email</div>
                        <div className='info-description'>{user?.email}</div>
                    </div>}
                    <span className='info-edit-button' onClick={() => setEditModalVisible(!editModalVisible)}>
                        <Edit />
                    </span>
                </div>
                <div className='profile-subtitle'>Profile settings</div>
                {settings.map((el, index) => (
                    <div key={index} className='profile-settings-block' onClick={() => navigate(`/dashboard/my-profile/${el.url}`)}>
                        <span className='profile-settings-block-title'>{el.title}</span>
                        <ArrowRight />
                    </div>
                ))}
            </div>
        )
    } else {
        return null
    }
}

export default Profile