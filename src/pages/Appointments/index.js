import React, { useState } from "react";
import { Button } from "../../components";

const Appointments = () => {
    const [tab, setTab] = useState('coming');

    return (
        <div>
            <div className='page-title'>Appointments</div>
            <div className="appointments-header-container">
                <div className="appointments-header-button-tabs">
                    <div className={`appointments-tab-button ${tab === 'coming' ? 'appointments-tab-button-active' : ''} coming-tab-button`} onClick={() => setTab('coming')}>Coming apointments (9)</div>
                    <div className={`appointments-tab-button ${tab === 'results' ? 'appointments-tab-button-active' : ''}`} onClick={() => setTab('results')}>Appointments results (5)</div>
                </div>
                <Button text={'Delay'} className={'delay-button'} />
            </div>
            {/* <div className="appointments-list-container">
                {[1, 2, 3, 4].map((el) => (
                    <div>
                        <div>Consultation</div>
                    </div>
                ))}
            </div> */}
        </div>
    )
}

export default Appointments;