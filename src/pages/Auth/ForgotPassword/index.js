
import React from 'react'
import BackArrow from '../../../assets/svg/back-arrow';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { forgotPasswordEmail } from '../../../store/user/actions';
import { userSelector } from '../../../store/user/selectors';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import {CustomLoader} from "../../../assets/loader/loader";

const initialStateUser = { email: '' };

const ForgotPassword = () => {
    const { errorForgotPassword } = useSelector(userSelector);
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const {isLoader}= useSelector(userSelector)

    const schema = yup.object({
        email: yup.string().email('Invalid email address!').required('Field is required!'),
    }).required();
    const { register, handleSubmit, formState: { errors }, reset } = useForm({
        resolver: yupResolver(schema),
        defaultValues: initialStateUser
    });

    const forgotPassword = (formValues) => {
        dispatch(forgotPasswordEmail({ ...formValues, navigate: () => { navigate('/forgot-password/email-verification'); reset() } }))
    }

    return (
        <>
            <div className="back-slide-container">
                <BackArrow />
                <span onClick={() => navigate(-1)}>Back</span>
            </div>
            <div className='centered-container' style={{ marginTop: 0 }}>
                {isLoader && <CustomLoader />}
                <div>
                    <span className='title'>Reset password</span>
                    <span style={{ marginTop: 14 }} className='description'>Enter the email that your account is registered to</span>
                    <form className='custom-form' style={{ marginTop: '7.5vh' }} onSubmit={handleSubmit(forgotPassword)}>
                        <div className='relative'>
                            <label
                                htmlFor='email'
                                className={`${errors.email || errorForgotPassword?.email ? 'error-color' : ''}`}>Email</label>
                            <input
                                {...register('email')}
                                className={`custom-input  ${errors.email || errorForgotPassword?.email ? 'error-input' : ''}`}
                                id='email'
                                name='email'
                                autoComplete='email'
                                placeholder={'Email'}
                            />
                            <p className='error-text'>{errors.email?.message || errorForgotPassword?.email}</p>
                        </div>
                        <div className='bottom-button-container' style={{ marginTop: '33vh' }}>
                            <button
                                className='button'
                                type='submit'
                            >
                                <p>Continue</p>
                            </button>
                        </div>
                        <span style={{ marginTop: '4vh', textAlign: "center" }} className='remember-your-password' >Remember your password? <span className='remember-password' onClick={() => navigate('/signin')}>Back to Log In</span></span>
                    </form>
                </div>
            </div>
        </>
    )
}

export default ForgotPassword