
import React from 'react'
import BackArrow from '../../../assets/svg/back-arrow';
import { useNavigate } from 'react-router-dom';
import Email from '../../../assets/svg/email';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';

const EmailVerification = () => {
    const navigate = useNavigate();

    return (
        <>
            <div className="back-slide-container">
                <BackArrow />
                <span onClick={() => navigate(-1)}>Back</span>
            </div>
            <div className='centered-container' style={{ marginTop: 0 }}>
                <div style={{maxWidth: 450}}>
                    <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center', marginBottom: '4vh'}}>
                        <Email />
                    </div>
                    <span className='title'>Check your email</span>
                    <span style={{ marginTop: 14 }} className='description'>We have sent you a verification email. Confirm your email to reset password.</span>

                    <span style={{ marginTop: '8.5vh', textAlign: 'center' }} className='remember-your-password' ><span className='remember-password' onClick={() => navigate('/signin')}>Back to Log In</span></span>
                </div>
            </div>
        </>
    )
}

export default EmailVerification