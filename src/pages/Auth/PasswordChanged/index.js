
import React from 'react'
import Button from '../../../components/Button'
import BackArrow from '../../../assets/svg/back-arrow';
import { useNavigate } from 'react-router-dom';

import Ok from '../../../assets/svg/ok';

const PasswordChanged = () => {
    const navigate = useNavigate();

    return (
        <>
            <div className="back-slide-container">
                <BackArrow />
                <span onClick={() => navigate(-1)}>Back</span>
            </div>
            <div className='centered-container' style={{ marginTop: 0 }}>
                <div >
                    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', marginBottom: '4vh' }}>
                        <Ok />
                    </div>
                    <span style={{ marginBottom: '7vh' }} className='title'>Password changed successfully!</span>
                    <div style={{ display: 'flex', justifyContent: 'center' }}>
                        <Button text={'Log In'} onClick={() => navigate('/signin')} />
                    </div>
                </div>
            </div>
        </>
    )
}

export default PasswordChanged