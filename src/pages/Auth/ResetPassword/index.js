
import React, { useEffect, useState } from 'react'
import Button from '../../../components/Button'

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { Input } from '../../../components';
import { useNavigate } from 'react-router-dom';
import Cancel from '../../../assets/svg/cancel';
import Close from '../../../assets/svg/close';
import Check from '../../../assets/svg/check';
import axios from 'axios';
import { API_URL } from '../../../constants';

const ResetPassword = () => {
    const [confirmPassword, setConfirmPassword] = useState('');
    const [errorModal, setErrorModal] = useState({ text: '', visible: false });

    let token = window.location.pathname.split('/')[3];
    const [password, setPassword] = useState('');
    const navigate = useNavigate();
    const [errors, setErrors] = useState(
        {
            characters: { title: '6 characters minimum', active: false },
            numbers: { title: 'At least 2 numbers', active: false },
            match: { title: 'Passwords should match', active: false },
            latin: { title: 'The password should include latin letters only', active: false }
        });


    useEffect(() => {
        axios.get(API_URL + 'check-token/' + token)
            .catch((error) => {
                // navigate('/password-changed');
                console.log('errrror', error.response.data)
                setErrorModal({ text: 'The token has expired, try again!', visible: true });
                // console.log('sdafasdfasdf')
            })
    }, [])

    useEffect(() => {
        if (password.length) {

            if (!/^[A-Za-z0-9]+$/.test(password)) {
                setErrors(prevState => ({ ...prevState, latin: { ...prevState.latin, active: false } }))
            } else {
                setErrors(prevState => ({ ...prevState, latin: { ...prevState.latin, active: true } }))
            }


            if (password.length < 6) {
                setErrors(prevState => ({ ...prevState, characters: { ...prevState.characters, active: false } }))
            } else {
                setErrors(prevState => ({ ...prevState, characters: { ...prevState.characters, active: true } }))
            }

            if (password.length - password.replace(/\d/gm, '').length < 2) {
                setErrors(prevState => ({ ...prevState, numbers: { ...prevState.numbers, active: false } }))
            } else {
                setErrors(prevState => ({ ...prevState, numbers: { ...prevState.numbers, active: true } }))
            }

            if (password !== confirmPassword) {
                setErrors(prevState => ({ ...prevState, match: { ...prevState.match, active: false } }))
            } else {
                setErrors(prevState => ({ ...prevState, match: { ...prevState.match, active: true } }))
            }
        } else {
            setErrors(prevState => ({ ...prevState, latin: { ...prevState.latin, active: false } }))
        }

    }, [password, confirmPassword]);

    const resetPassword = () => {
        axios.post(API_URL + 'new-password/' + token, { token, password })
            .then((response) => {
                navigate('/password-changed');
            })
    }

    return (
        <div className='centered-container'>
            {errorModal.visible && <div className='modal-container' style={{ left: 0 }}>
                <div style={{ backgroundColor: 'white', padding: 30, borderRadius: 10 }}>
                    <div className='edit-profile-title'>{errorModal.text}</div>
                    <div className='centered-block'>
                        <Button text={'Log In'} style={{ marginTop: 25 }} onClick={() => navigate('/signin')} />
                    </div>
                </div>
            </div>}
            <div>
                <span className='title'>Reset password</span>
                <span style={{ marginTop: 14 }} className='description'>Create new password</span>
                <Input isPassword containerStyle={{ marginTop: 75 }} placeholder={'Password'} onChange={setPassword} value={password} title={'New password'} type={'password'} />
                <Input isPassword containerStyle={{ marginTop: 45 }} placeholder={'Confirm password'} onChange={setConfirmPassword} value={confirmPassword} title={'Confirm password'} type={'password'} />
                <div>
                    {Object.entries(errors).map((el, index) => (
                        <div style={{ marginTop: '2.5vh', display: 'flex', alignItems: 'center' }}>
                            {!el[1].active ? <Cancel /> : <Check />}
                            <span className='errors-password' style={{ color: el[1].active ? '#414141' : '#ACACAC' }}>{el[1].title}</span>
                        </div>
                    ))}
                </div>
                <div className='bottom-button-container' style={{ marginTop: '4.5vh' }}>
                    <Button text={'Continue'} disabled={Object.entries(errors).filter(el => !el[1].active).length} onClick={resetPassword} />
                </div>
            </div>
        </div>
    )
}

export default ResetPassword