import React, { useEffect, useState } from 'react'
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { userLogin } from '../../../store/user/actions';
import { userSelector } from '../../../store/user/selectors';
import HidePassword from '../../../assets/svg/hidePassword';
import ShowPassword from '../../../assets/svg/showPassword';
import Close from '../../../assets/svg/close';
import CircleCancel from '../../../assets/svg/CircleCancel';
import { userSlice } from '../../../store/user';
import {CustomLoader} from "../../../assets/loader/loader";

const Login = () => {
    const schema = yup.object({
        email: yup.string().email('Invalid email address!').required('Field is required!'),
        password: yup.string().required('Field is required!')
    }).required();
    const { register, handleSubmit, formState: { errors }, reset } = useForm({
        resolver: yupResolver(schema)
    });

    const { errorLogin, isLoader } = useSelector(userSelector);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const [errorLoginModal, setErrorLoginModal] = useState({ text: '', visible: false });
    const [passwordShown, setPasswordShown] = useState(false);

    useEffect(() => {
        if (errorLogin?.email) {
            setErrorLoginModal({ text: errorLogin.email, visible: true });
        } else if (errorLogin?.password) {
            setErrorLoginModal({ text: errorLogin.password, visible: true });
        }

    }, [errorLogin])

    const login = (formValues) => {
        dispatch(userLogin({ ...formValues, navigate: () => { navigate('../dashboard/my-profile'); reset() } }))
    }

    const togglePasswordVisiblity = () => {
        setPasswordShown(!passwordShown);
    };

    const closeModalOnIcon = () => {
        dispatch(userSlice.actions.resetErrorLogin());
        setErrorLoginModal({ text: '', visible: !errorLoginModal.visible })
    }
    return (
        <div className='centered-container'>
            {isLoader && <CustomLoader/>}
            <div>
                {errorLoginModal.visible && <div className='modal-container' style={{ left: 0 }}>
                    <div style={{ backgroundColor: 'white', padding: 30, borderRadius: 10 }}>
                        <div
                            className='close-button-modal-block'
                            onClick={closeModalOnIcon}>
                            <span><Close /></span>
                        </div>
                        <div style={{ display: 'flex', justifyContent: 'center', marginBottom: 25 }}>
                            <CircleCancel />
                        </div>
                        <div className='user-not-defined-text'>
                            {errorLoginModal.text}
                        </div>
                    </div>
                </div>}
                <span className='title'>Welcome back!</span>
                <span style={{ marginTop: 14 }} className='description'>We are glad to see you again!</span>
                <form
                    className='custom-form'
                    style={{ marginTop: '7.5vh' }}
                    onSubmit={handleSubmit(login)}
                >
                    <div className='relative'>
                        <label
                            htmlFor='email'
                            className={`${errors?.email ? 'error-color' : ''}`}>Email</label>
                        <input
                            {...register('email')}
                            className={`custom-input  ${errors?.email || errorLogin?.email ? 'error-input' : ''}`}
                            id='email'
                            name='email'
                            autoComplete='email'
                            placeholder={'Email'}
                        />
                        <p className='error-text'>{errors.email?.message}</p>
                    </div>
                    <div className='relative'>
                        <label
                            htmlFor='password'
                            className={`${errors.password ? 'error-color' : ''}`}>Password</label>
                        <input
                            {...register('password')}
                            className={`custom-input ${errors.password || errorLogin?.password ? 'border-error' : ''}`}
                            id='password'
                            name='password'
                            type={passwordShown ? "text" : "password"}
                            autoComplete='password'
                            placeholder={'Password'}
                        />
                        <span className='visible-password-icon' onClick={togglePasswordVisiblity} > {passwordShown ? <HidePassword /> : <ShowPassword />}</span>
                        <p className='error-text'>{errors.password?.message}</p>
                    </div>
                    <span className='forgot-password' onClick={() => navigate('/forgot-password')} >Forgot password?</span>
                    <div className='bottom-button-container'>
                        <button
                            className='button'
                            type='submit'
                        >
                            <p>Sign In</p>
                        </button>
                    </div>
                </form>
            </div >
        </div >
    )
}

export default Login
